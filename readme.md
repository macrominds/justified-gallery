# @macrominds/justified-gallery

Pure CSS justified gallery: Aligned images, scaled to fit in a rectangular area.

This is a pure css variant of the popular justified gallery.
It aligns images of different proportions to fit into a rectangular area.

It is not as reliable as a javascript implementation, but works with a surprisingly large number of variants and is orders of magnitude smaller than the Javascript implementations. Plus: it leverages the browser's native ability to lay out the images; hence it is very performant.

